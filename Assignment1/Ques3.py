
# Write a Python program to construct the following pattern, using a nested for loop.

# * 
# * * 
# * * * 
# * * * * 
# * * * * * 
# * * * * 
# * * * 
# * * 
# *

number_of_rows = int(input("Enter odd number > 0 : "))

for row in range(1, number_of_rows+1):
    if row <= (number_of_rows+1)/2:
        stars = row+1
    else:
        stars -= 1
    for _ in range(1, stars):
        print('*', end=' ')
    
    print()
