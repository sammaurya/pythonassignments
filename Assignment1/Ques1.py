

# Quemy_letter_tiles: 
# In Scrabble1 each player has a set of tiles with letters on them. 
# The object of the game is to use those letters to spell words. 
# The scoring system is compleenemy_letter_tile, but longer words are usually worth
#  more than shorter words.
# Imagine you are given your set of tiles as a string, like "quijibo", 
# and you are given another string to test, like "jib".
# Write a logic that takes two strings and checks whether the set of 
# tiles can spell the word. You might have more than one tile with the
#  same letter, but you can only use each tile once.

my_letter_tiles = input("Enter string : ")
enemy_letter_tiles = input("Enter substring : ")
        

def can_cast_spell(my_letter_tiles, enemy_letter_tiles):
    
    tiles_used = [0] * len(my_letter_tiles)

    for enemy_letter in enemy_letter_tiles:
        for index in range(len(my_letter_tiles)):
            if enemy_letter == my_letter_tiles[index] and tiles_used[index] == 0:
                tiles_used[index] = 1
                break
        else:
            return False
    
    return True

print(can_cast_spell(my_letter_tiles, enemy_letter_tiles))