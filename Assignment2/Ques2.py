
# Ques2:
# Given an array of names of candidates in an election. 
# A candidate name in array represents a vote casted to the candidate. 
# Print the name of candidates received max_votes vote. 
# If there is a tie, print lexicographically smaller name.

# Examples:

# Input :  votes[] = {"john", "johnny", "jackie", "johnny", "john", "jackie", "jamie", "jamie", "john", "johnny", "jamie", "johnny", "john"};
# Output : John

# We have four Candidates with name as 'John', 
# 'Johnny', 'jamie', 'jackie'. The candidates
# John and Johnny get max_votesimum votes. Since John
# is alphabetically smaller, we print it.


vote_cast = ["john", "johnny", "jackie", "johnny",
         "john", "jackie", "jamie", "jamie", 
         "john",  "johnny", "jamie", "johnny", "john"]

votes_count = {}

for vote_to_candidate in vote_cast:
    if vote_to_candidate in votes_count.keys():
        votes_count[vote_to_candidate] += 1
    else:
        votes_count[vote_to_candidate] = 1

winner = ""
max_votes = 0

for candidate, votes in votes_count.items():
    
    if votes > max_votes:
        winner = candidate
        max_votes = votes

    elif votes == max_votes:
        if winner > candidate:
            winner = candidate
    
    
print(winner)
     
