
# Ques1: 
# Given a list of tuples, the task is to remove all tuples having duplicate tuple_ values from the given list of tuples.
# input_list:  [('Tuple1', 121), ('Tuple2', 125), ('Tuple1', 135), ('Tuple4', 478)]
# Output:  [('Tuple1', 121), ('Tuple2', 125), ('Tuple4', 478)]

input_list =  [('Tuple1', 121), ('Tuple2', 125), ('Tuple1', 135), ('Tuple4', 478)]

exiting_tuple_list = []
out_list = list(input_list)

for tuple_number, value in input_list:
    if tuple_number in exiting_tuple_list:
        to_remove = (tuple_number,value)
        out_list.remove(to_remove)
    else:
        exiting_tuple_list.append(tuple_number)

print(out_list)