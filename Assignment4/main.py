
# Ques 2:
# Write a main.py file that import these functions and show the use of them.

from my_date import date_op

birth_date = input("Enter birth date (yyyy/mm/dd) : ")

birth_date = "".join(char if char.isdigit()
                     else " " for char in birth_date).split()

year = int(birth_date[0])
month = int(birth_date[1])
day = int(birth_date[2])


if not date_op.is_valid_date(year, month, day):
    print("Invalid date!!!")
    exit()
    
birth_date = date_op.datetime(year, month, day)

print("Today is ", date_op.get_current_datetime())

age = date_op.get_age_from_birth_date(birth_date)

print("Age is ", age)

print("Date of birth is ", date_op.get_birth_date_from_age(age))

print("Time until 50 is ", date_op.time_to_turn_50(birth_date))
