
# write a python program to create a text file containing countings of 
# all the numbers from 1 to 20 in this  format :
#  	2 : 2, 4, 6, 8, 10, 12, 14, 16, 18, 20
#   3 : 3, 6, 9, 12, 15, 18, 21, 24, 27, 30 and so on.
#
with open("table_file.txt", "w") as table_file:
    num_table = ""
    for number in range(1, 21):

        num_table = num_table + "{} : ".format(number)
        for times in range(number, number*10+1, number):
            num_table = num_table + "{}, ".format(times)
        num_table += "\n"

    table_file.write(num_table)