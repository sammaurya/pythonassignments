
# Write a python program to load the dictionary created above 
# and then take user input and then print table list on console.

import pickle

with open("table_dict.pickle", "rb") as pickle_file:
    number_dict = pickle.load(pickle_file)
    number = int(input("Enter a number [1-40] : "))

    try:
        print(number_dict[number][:10])
    except:
        print("Out of Range")