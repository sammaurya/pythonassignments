

# write another script to append counting of next 20 numbers. I.re 21 - 40

with open("table_file.txt", "a") as table_file:
    num_table = ""
    for number in range(21, 41):
        
        num_table = num_table + "{} : ".format(number)
        for times in range(number, number*10+1, number):   
               
            num_table = num_table + "{}, ".format(times)
        num_table += "\n"

    table_file.write(num_table)
