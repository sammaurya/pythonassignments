
# Once done with creating the dictionary file then dump the dictionary object using pickle.

import pickle

with open("table_file.txt", "r") as table_file:

    num_table_dict = {}

    for line in table_file:
        number_and_values_list = line.split(":")
        number = int(number_and_values_list[0].strip())
        values = [ i.strip() for i in number_and_values_list[1].split(",")]
        num_table_dict[number] = values

    with open("table_dict.pickle", "wb") as pickle_file:
        pickle.dump(num_table_dict, pickle_file)


