
# Read the file created above and load it into dictionary in the format given below  
# num_table_dictionary =  {
# 2 : [2, 4, 6, 8, 10..]
# 3 : [3, 6, 9, 12..]
# }
# Note- Do not make dictionary using loop and multiplication. Make it using reading 
# the text from the file and then use string methods.


with open("table_file.txt", "r") as table_file:

    num_table_dict = {}

    for line in table_file:
        number_and_value_list = line.split(" : ")
        number = int(number_and_value_list[0].strip())
        values = [ int(value) for value in number_and_value_list[1].split(", ")[:10]]
        num_table_dict[number] = values

    for number, values in num_table_dict.items():
        print(number, values)